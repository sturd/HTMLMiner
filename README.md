
Execute HTMLCOIN
===

    htmlcoin[d|-qt] -server -rpcbind=127.0.0.1 -rpcport=4889 -rpcthreads=8 -rpcuser=[USER] -rpcpassword=[PASSWORD]


Environment
===

    $HTML_RPC_USER = [USER]
    $HTML_RPC_PASS = [PASSWORD]