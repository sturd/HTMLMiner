package main

import (
	"github.com/sturd/btcd/rpcclient"
	"fmt"
	"os"
	"strings"
	"strconv"
	"time"
)

const(
	cHTML_RPC_USER = "HTML_RPC_USER"
	cHTML_RPC_PASS = "HTML_RPC_PASS"
)


func MinerThread(rpcHost string) {
	cfg := &rpcclient.ConnConfig{
		Host: rpcHost,
		HTTPPostMode: true,
		DisableTLS: true,
		User: os.Getenv(cHTML_RPC_USER),
		Pass: os.Getenv(cHTML_RPC_PASS),
	}

	client, err := rpcclient.New(cfg, nil)
	if err != nil {
		panic(err)
	}

	defer client.Disconnect()

	for {
		hashes, err := client.Generate(100)
		if err != nil {
			panic(err)
		}

		if len(hashes) > 0 {
			for h := range hashes {
				fmt.Println("Mined block: " + hashes[h].String())
			}
		}
	}
}

func main() {

	var err error
	var threads int64 = 0
	len := len(os.Args)
	for i := 0; i < len; i++ {
		if strings.Compare(os.Args[i], "-t") == 0 {
			if len-1 >= i {
				threads, err = strconv.ParseInt(os.Args[i+1], 10, 64)
				if err != nil {
					panic(err)
				}
			}
		}
	}

	for i := 0; i < int(threads); i++ {
		fmt.Println("Launched thread: " + strconv.FormatInt(int64(i), 10))
		go MinerThread("127.0.0.1:4889")
	}

	t := time.NewTicker(5 * time.Second)
	for new := range t.C {
		new.Day()
	}
}
